package com.example.other;

import lombok.Data;


/**
 * @author 沈传尚
 * @date 2021/12/17
 */
@Data
public class Student extends Teacher {
    private String name;
    private Integer age;
    private String bir;
}
