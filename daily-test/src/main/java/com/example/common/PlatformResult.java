package com.example.common;



import com.alibaba.fastjson.JSON;

import java.util.HashMap;

/**
 * @author 沈传尚
 * @date 2022/3/9
 */
public class PlatformResult<T> extends Result{
    private static final long serialVersionUID = -4759913324592501472L;

    private Integer code;

    private String msg;

    private T data;

    public static PlatformResult success(Object... o) {
        PlatformResult result = new PlatformResult();
        result.setCode(10000);
        HashMap<String, Object> data = new HashMap();

        for(int i = 0; i < o.length; ++i) {
            System.out.println(o);
            String str = o[i].getClass().getSimpleName();
            String s = str.substring(0, 1).toLowerCase() + str.substring(1);
            data.put(s, o[i]);
        }

        result.setData(data);
        return result;
    }


    public static PlatformResult success(Object o) {
        PlatformResult result = new PlatformResult();
        result.setCode(ResultCodeEnum.SUCCESSFUL.getCode());
        result.setData(o);
        return result;
    }

    public static String fail(String message) {
        PlatformResult result = new PlatformResult();
        result.setCode(ResultCodeEnum.FAIL.getCode());
        result.setMsg(message);
        return JSON.toJSONString(result);
    }

    public PlatformResult successful(T o) {
        this.setCode(ResultCodeEnum.SUCCESSFUL.getCode());
        this.setMsg(ResultCodeEnum.SUCCESSFUL.getMsg());
        this.setData(o);
        return this;
    }

    public static PlatformResult failure(String message) {
        PlatformResult result = new PlatformResult();
        result.setCode(ResultCodeEnum.FAIL.getCode());
        result.setMsg(message);
        return result;
    }

    public static PlatformResult victory(String message) {
        PlatformResult result = new PlatformResult();
        result.setCode(ResultCodeEnum.SUCCESSFUL.getCode());
        result.setMsg(message);
        return result;
    }

    public static <T> PlatformResult.PlatformResultBuilder<T> builder() {
        return new PlatformResult.PlatformResultBuilder();
    }

    public PlatformResult() {
    }

    public PlatformResult(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Integer getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

    public T getData() {
        return this.data;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof PlatformResult)) {
            return false;
        } else {
            PlatformResult<?> other = (PlatformResult)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$code = this.getCode();
                    Object other$code = other.getCode();
                    if (this$code == null) {
                        if (other$code == null) {
                            break label47;
                        }
                    } else if (this$code.equals(other$code)) {
                        break label47;
                    }

                    return false;
                }

                Object this$msg = this.getMsg();
                Object other$msg = other.getMsg();
                if (this$msg == null) {
                    if (other$msg != null) {
                        return false;
                    }
                } else if (!this$msg.equals(other$msg)) {
                    return false;
                }

                Object this$data = this.getData();
                Object other$data = other.getData();
                if (this$data == null) {
                    if (other$data != null) {
                        return false;
                    }
                } else if (!this$data.equals(other$data)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof PlatformResult;
    }

//    public int hashCode() {
//        int PRIME = true;
//        int result = 1;
//        Object $code = this.getCode();
//        int result = result * 59 + ($code == null ? 43 : $code.hashCode());
//        Object $msg = this.getMsg();
//        result = result * 59 + ($msg == null ? 43 : $msg.hashCode());
//        Object $data = this.getData();
//        result = result * 59 + ($data == null ? 43 : $data.hashCode());
//        return result;
//    }

    public String toString() {
        return "PlatformResult(code=" + this.getCode() + ", msg=" + this.getMsg() + ", data=" + this.getData() + ")";
    }

    public static class PlatformResultBuilder<T> {
        private Integer code;
        private String msg;
        private T data;

        PlatformResultBuilder() {
        }

        public PlatformResult.PlatformResultBuilder<T> code(Integer code) {
            this.code = code;
            return this;
        }

        public PlatformResult.PlatformResultBuilder<T> msg(String msg) {
            this.msg = msg;
            return this;
        }

        public PlatformResult.PlatformResultBuilder<T> data(T data) {
            this.data = data;
            return this;
        }

        public PlatformResult<T> build() {
            return new PlatformResult(this.code, this.msg, this.data);
        }

        public String toString() {
            return "PlatformResult.PlatformResultBuilder(code=" + this.code + ", msg=" + this.msg + ", data=" + this.data + ")";
        }
    }

}
