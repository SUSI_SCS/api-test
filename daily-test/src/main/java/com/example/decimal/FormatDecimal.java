package com.example.decimal;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author susi
 * @date 2022/5/23
 */
public class FormatDecimal {
    public static void main(String[] args) {
        BigDecimal pass=new BigDecimal(2);
        BigDecimal join=new BigDecimal(3);
        String s = pass.divide(join, 2, RoundingMode.HALF_UP).toString();
        System.out.println(s);
    }
}
