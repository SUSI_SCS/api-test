package com.example.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author 沈传尚
 * @date 2022/1/24
 */
public class ReduceTest2 {
    public static void main(String[] args) {
        List<User> list= Arrays.asList(User.builder().age(18).build(),User.builder().age(45).build(),User.builder().age(15).build(),User.builder().age(5).build());

        // 求年龄最大值
        Integer max = list.stream().map(User::getAge).reduce(Integer::max).get();

        //  求最小值
        Integer min = list.stream().map(User::getAge).reduce(Integer::min).get();

        // 求平均值
        Integer sum = list.stream().map(User::getAge).reduce(Integer::sum).get();

        System.out.println("max="+max+";min="+min+";sum="+sum);
    }
}
