package com.example.autocode.mapper;

import com.example.autocode.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 沈传尚
 * @since 2022-01-22
 */
public interface UserMapper extends BaseMapper<User> {

}
