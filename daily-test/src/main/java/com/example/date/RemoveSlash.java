package com.example.date;

/**
 * @author 沈传尚
 * @date 2022/2/25
 */
public class RemoveSlash {
    public static void main(String[] args) {
        Demo demo=new Demo();
        demo.setDept("总监(张闵)/财务部");

       demo.setDept(removeSlash(demo.getDept()));
        System.out.println(demo.getDept());

    }

    public static String removeSlash(String s){
        if (s!=null){
          return   s.substring(s.indexOf("/")+1);
        }
        return "";
    }
}
