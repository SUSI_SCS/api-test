package com.example.other;

import java.lang.reflect.Field;

/**
 * @author 沈传尚
 * @date 2021/12/17
 */
public class FieldsTest {
    public static void main(String[] args) {
        Class s=Student.class;
        /**
         *  通过反射来获取对象的属性
         */
        for (Field f : s.getDeclaredFields()){
            f.setAccessible(true);
            System.out.println(f);
        }

//        for (Field f : s.getClass().getFields()){
//            f.setAccessible(true);
//            System.out.println(f);
//        }

    }
}
