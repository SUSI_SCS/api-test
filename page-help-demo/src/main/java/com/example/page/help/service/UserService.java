package com.example.page.help.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.page.help.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 沈传尚
 * @since 2022-01-22
 */
public interface UserService extends IService<User> {

}
