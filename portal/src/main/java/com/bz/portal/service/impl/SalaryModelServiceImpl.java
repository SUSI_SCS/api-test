package com.bz.portal.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bz.portal.entity.SalaryModel;
import com.bz.portal.mapper.SalaryModelMapper;
import com.bz.portal.service.SalaryModelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bz.portal.service.ex.NotFoundDataException;
import com.bz.portal.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 沈传尚
 * @since 2022-01-17
 */
@Service
public class SalaryModelServiceImpl extends ServiceImpl<SalaryModelMapper, SalaryModel> implements SalaryModelService {
    @Autowired(required = false)
    private SalaryModelMapper salaryModelMapper;


    @Override
    public JsonResult exceptionTest() {
        return new JsonResult(new NotFoundDataException("数据找不到异常!"));

    }

    @Override
    public List<SalaryModel> listAll(Integer type) {
        JsonResult<List<SalaryModel>> result=new JsonResult();
        // 职能岗薪酬表列表展示
        QueryWrapper<SalaryModel> wrapper=new QueryWrapper<>();
        wrapper.eq("type", type);
        List<SalaryModel> salaryModels = salaryModelMapper.selectList(wrapper);
        return salaryModels;
    }

}
