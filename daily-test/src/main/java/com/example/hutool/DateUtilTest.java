package com.example.hutool;



import cn.hutool.core.date.DateUtil;

import java.time.LocalDateTime;

/**
 * @author 沈传尚
 * @date 2021/12/16
 */
public class DateUtilTest {
    public static void main(String[] args) {
        /**
         * 给定一个固定时间 获取该时间和当前时间的差值
         */
        LocalDateTime deadLine=LocalDateTime.now();
        long l= DateUtil.parse(DateUtil.format(deadLine,"yyyy-MM-dd HH:mm:ss")).getTime() - System.currentTimeMillis();
        System.out.println(l);
    }
}
