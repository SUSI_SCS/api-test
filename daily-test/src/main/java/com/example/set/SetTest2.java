package com.example.set;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author 沈传尚
 * @date 2021/12/23
 */
public class SetTest2 {
    public static void main(String[] args) {
        List<Set<Integer>> setList=new ArrayList<>();
        Set<Integer> set1=new HashSet<>();
        Set<Integer> set2=new HashSet<>();
        set1.add(5);
        set2.add(4);
        set1.add(8);
        set2.add(7);
        setList.add(set1);
        setList.add(set2);
        for (Set<Integer> set : setList){

        }
    }
}
