package com.example.stream;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author 沈传尚
 * @date 2022/1/26
 */
public class BigDecimalTest {
    public static void main(String[] args) {
        List<User> list= Arrays.asList(
                User.builder().salary(new BigDecimal("8000")).build(),
                User.builder().salary(new BigDecimal("7500")).build(),
                User.builder().salary(new BigDecimal("9500")).build());

        // 最高薪资
        BigDecimal max = list.stream().map(User::getSalary).max((x1, x2) -> x1.compareTo(x2)).get();

        BigDecimal min = list.stream().map(User::getSalary).min(BigDecimal::compareTo).get();

        System.out.println(max);
        System.out.println(min);
    }
}
