package com.example.easy.excel.excelreadnew;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

/**
 * @author 沈传尚
 * @date 2022/2/19
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ReadOneOrMoreTest {

    @Test
    public void readOneTest(){
        int year = LocalDateTime.now().getYear();
        System.out.println(year);

    }


    @Test
    public void readMoreTest(){

    }


}
