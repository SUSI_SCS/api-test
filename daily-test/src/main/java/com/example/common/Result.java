package com.example.common;

import java.io.Serializable;

/**
 * 重载和重写的区别有以下几点：
 * 一、定义上的区别：
 * 1、重载是指不同的函数使用相同的函数名，但是函数的参数个数或类型不同。调用的时候根据函数的参数来区别不同的函数。
 * 2、覆盖（也叫重写）是指在派生类中重新对基类中的虚函数（注意是虚函数）重新实现。即函数名和参数都一样，只是函数的实现体不一样。
 *
 *
 */
public class Result implements Serializable {
    /**
     * 方法重载
     */
    public Result() {
    }


    public Result(int a){

    }
}
