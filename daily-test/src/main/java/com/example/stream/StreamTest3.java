package com.example.stream;

import com.example.entity.Point;
import com.google.common.collect.Lists;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @author 沈传尚
 * @date 2022/1/5
 */
public class StreamTest3 {
    public static void main(String[] args) {
        Point p0 = Point.builder().x(0).y(4).build();
        Point p1 = Point.builder().x(5).y(4).build();
        Point p2 = Point.builder().x(3).y(0).build();
        Point p4 = Point.builder().x(7).y(1).build();
        Point p3 = Point.builder().x(1).y(0).build();
        List<Point> list=new ArrayList<>();
        list.add(p0); list.add(p1); list.add(p2); list.add(p3); list.add(p4);

        // 使用filter() 过滤列表数据 filter(T -> boolean)
        List<Point> collect = list.stream().filter((point) -> point.getX() == 1).collect(Collectors.toList());
        collect.forEach(System.out::println);

        // findFirst()  获取第一条数据
        Point point = list.stream().filter(p -> p.getY() == 0).findFirst().orElse(null);
        System.out.println(point);

        // findAny() 串行流结果与findFirst()相同 并行流一定不同
        Point point1 = list.parallelStream().findAny().orElse(null);
        System.out.println(point1);

        // map(T -> R) 使用map()将流中的每一个元素T 映射为R
        List<Integer> l1 = list.stream().map(Point::getX).collect(Collectors.toList());
        System.out.println(l1);

        // flatMap(T -> Stream) 将流中的每一个元素T映射为一个流 再把每一个流连接成为一个流. (总-> 分 -> 总)
        ArrayList<String> strings = Lists.newArrayList("java", "python", "php");
        System.out.println(strings);
        strings.stream().map(e->e.split("")).distinct().map(Arrays::toString).collect(Collectors.toList()).forEach(System.out::println);







    }
}
