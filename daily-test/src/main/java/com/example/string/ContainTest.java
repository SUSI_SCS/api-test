package com.example.string;

import java.util.Arrays;
import java.util.List;

/**
 * @author 沈传尚
 * @date 2022/3/3
 */
public class ContainTest {
    public static void main(String[] args) {
        String s1="2024-09-08";
        String s2="2021-09-07";
        Integer diff = diff(s2, s1);
        System.out.println(diff);


    }

    public static Integer  diff(String s1, String s2){
        Integer i1=Integer.parseInt(s1.substring(0, s1.indexOf("-")));
        Integer i2=Integer.parseInt(s2.substring(0, s2.indexOf("-")));
        return Math.abs(i1-i2);
    }
}
