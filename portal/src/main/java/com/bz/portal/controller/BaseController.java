package com.bz.portal.controller;

import com.bz.portal.service.ex.NotFoundDataException;
import com.bz.portal.service.ex.ServiceException;
import com.bz.portal.util.JsonResult;
import org.springframework.web.bind.annotation.ExceptionHandler;

/** 控制器的积累*/
public class BaseController {
    public  static final int OK=200;


    @ExceptionHandler({ServiceException.class})
    public JsonResult<Void> handleException(Throwable e){
        JsonResult<Void> result=new JsonResult<>(e);
        if (e instanceof NotFoundDataException){
            result.setState(4000);
        }
        return  result;
    }




}
