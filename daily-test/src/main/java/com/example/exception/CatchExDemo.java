package com.example.exception;

import com.example.common.PlatformResult;

/**
 * @author 沈传尚
 * @date 2022/3/9
 */
public class CatchExDemo {
    public static void main(String[] args) {
        Integer x=1;
        try {
            if (x == 1){
                throw new RuntimeException("出错了");
            }
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }
}
