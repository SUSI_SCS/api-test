package com.example.iterable.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * 集合间的操作 addAll  containsAll  removeAll
 */
public class CollectionTest2 {
    public static void main(String[] args) {
        List<Integer> integerList=new ArrayList<>(Arrays.asList(8,1));

        List<Integer> integers=new ArrayList<>(Arrays.asList(5,1,6,10));

        // 1.判断给定的元素是否在集合中
        boolean result1 = integerList.contains(1);

        // 2.判断一个集合的所有元素是否被另一个集合包含
        boolean result2 = integers.containsAll(integerList);
        System.out.println("result1:"+result1);
        System.out.println("result2:"+result2);

        // 3.将集合integers中的所有元素添加到integerList集合中 integerList集合发生了变化返回的就是true
        boolean b = integerList.addAll(integers);
        System.out.println(b);
        System.out.println(integerList);

        // 4.删除integers集合中与integerList集合相同的元素 删除了元素就返回true 没有删除元素就是false
        boolean result = integers.removeAll(integerList);
        System.out.println(result);
        System.out.println(integers);
    }
}
