package com.example.xssfworkbook;

import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.*;

import java.awt.*;

/**
 * @author 沈传尚
 * @date 2022/3/1
 */
public class Test {
    public static void main(String[] args) {
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setFillBackgroundColor(new XSSFColor(new Color(192,192,192)));
        XSSFSheet sheet = wb.createSheet("测试数据表");
        XSSFRow row = null;
        XSSFCell cell = row.createCell(0);
        cell.setCellStyle(cellStyle);

    }
}
