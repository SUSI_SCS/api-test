package com.example.easy.excel.excelwriteold;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 沈传尚
 * @date 2022/2/19
 */
public class ExcelWriteWithoutHeadTest {
    public static void main(String[] args)  {
        writeWithoutHead();
    }

    /**
     * 没行数据是List<String> 没有表头
     */
    public static void writeWithoutHead() {
        try(OutputStream out=new FileOutputStream("withoutHead.xlsx");){
            ExcelWriter writer=new ExcelWriter(out, ExcelTypeEnum.XLSX,false);
            Sheet sheet1=new Sheet(1,0); // 指定sheet页 从1开始; 指定开始行 从0开始
            sheet1.setSheetName("sheet2");
            List<List<String>> data=new ArrayList<>();
            for (int i = 0; i < 100; i++) {
                List<String> item=new ArrayList<>();
                item.add("item"+i);
                item.add("item"+i);
                item.add("item"+i);
                data.add(item);
            }
            writer.write0(data,sheet1);
            writer.finish();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

}
