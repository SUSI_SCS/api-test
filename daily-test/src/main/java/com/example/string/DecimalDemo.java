package com.example.string;

import cn.hutool.core.util.NumberUtil;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * @author susi
 * @date 2022/6/24
 */
public class DecimalDemo {
    public static void main(String[] args) {
        String s = new BigDecimal(String.valueOf(2)).divide(new BigDecimal(String.valueOf(3)), 2, BigDecimal.ROUND_HALF_UP)
                .multiply(new BigDecimal(100)).toPlainString();

        String substring = s.substring(0, s.indexOf("."));

        System.out.println(substring);


        String s1 = new BigDecimal(String.valueOf(1)).divide(new BigDecimal(String.valueOf(21)), 2, BigDecimal.ROUND_HALF_UP).toString();





        System.out.println(s);
    }
}
