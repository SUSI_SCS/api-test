package com.example.entity;

import lombok.Builder;
import lombok.Data;

/**
 * @author 沈传尚
 * @date 2022/2/18
 */
@Data
@Builder
public class Area {
    private String province;

    private String city;
}
