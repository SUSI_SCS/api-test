package com.example.iterable.collection;

import java.util.*;

/**
 * @author 沈传尚
 * @date 2021/12/31
 */
public class Test {
    public static void main(String[] args) {
        String s = get(10);
        System.out.println(s);


    }

    public static String get(Integer x){
        switch (x){
            case 0:
                return "进行中";
            case 1:
                return "审批中";
            case 2:
                return "已完成";
            default:
                return "关闭";
        }
    }

}
