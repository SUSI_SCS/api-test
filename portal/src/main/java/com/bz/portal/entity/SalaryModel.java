package com.bz.portal.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 沈传尚
 * @since 2022-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SalaryModel implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 职级名称
     */
    private String positionName;

    /**
     * 职级等级 M级 管理层
     */
    private String managerLevel;

    /**
     * 职级等级 P级   执行层
     */
    private String professionLevel;

    /**
     * 基准(基础薪酬系数)
     */
    private Integer basicCoefficient;

    /**
     * 1档(下限)
     */
    private String firstLevel;

    /**
     * 2档(标准)
     */
    private String secondLevel;

    /**
     * 3档(上限)
     */
    private String thirdLevel;

    /**
     * 4档(预留字段)
     */
    private String fourthLevel;

    /**
     * 5档(预留字段)
     */
    private String fifthLevel;

    /**
     * 6档(预留字段)
     */
    private String sixthLevel;

    /**
     * 备注
     */
    private String comment;

    /**
     * 岗位类型 (0 职能岗 1 研发岗)
     */
    private Integer type;


}
