package com.example.stream;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author 沈传尚
 * @date 2022/1/8
 */
public class JudgeStreamTest {
    public static void main(String[] args) {
        // anyMatch(T -> boolean) 判断流中是否有一个元素匹配给定的(T -> boolean)条件
        List<Integer> list = Arrays.asList(2, 10, 5, 8, 9, 0, 1);
        boolean b = list.stream().anyMatch(Predicate.isEqual(0));
        System.out.println(b);

    }
}
