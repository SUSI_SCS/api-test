package com.example.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author susi
 * @date 2022/6/24
 */
public class PeekDemo {
    public static void main(String[] args) {
        List<User> list= Arrays.asList(
                User.builder().deptName("开发部").age(20).build(),
                User.builder().deptName("测试部").age(18).build(),
                User.builder().deptName("开发部").age(32).build(),
                User.builder().deptName("开发部").age(25).build()
        );


        List<User> users = list.stream().peek(user -> {
            user.setAge(1000);
        }).collect(Collectors.toList());


        users.forEach(e-> System.out.println(e.getAge()));
    }
}
