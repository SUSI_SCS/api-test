package com.example.easy.excel3;

import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.util.StyleUtil;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import org.apache.poi.ss.usermodel.*;

import java.util.List;

/**
 * @author 沈传尚
 * @date 2022/3/15
 */
public class RowWriteHandler implements CellWriteHandler {
    @Override
    public void beforeCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row,
                                 Head head, Integer integer, Integer integer1, Boolean aBoolean) {

    }

    @Override
    public void afterCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Cell cell,
                                Head head, Integer integer, Boolean aBoolean) {

    }

    @Override
    public void afterCellDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder,
                                 List<CellData> list, Cell cell, Head head, Integer integer, Boolean aBoolean) {
        int i = cell.getColumnIndex();
        writeSheetHolder.getSheet().getRow(cell.getRowIndex()).setHeight((short) (2.34 * 256));
        if (1 == cell.getRowIndex()) {
            // 根据单元格获取workbook
            Workbook workbook = cell.getSheet().getWorkbook();
            //设置行高
//            writeSheetHolder.getSheet().getRow(cell.getRowIndex()).setHeight((short) (1.4 * 256));
            writeSheetHolder.getSheet().setColumnWidth(i, (short) (13.18 * 256));
            // 单元格策略
            WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
            // 设置背景颜色白色
            contentWriteCellStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
            WriteFont contentWriteFont = new WriteFont();
            //内容字体大小
            contentWriteFont.setFontName("宋体");
            contentWriteFont.setBold(false);
            contentWriteFont.setFontHeightInPoints((short) 12);
            contentWriteCellStyle.setWriteFont(contentWriteFont);
            CellStyle cellStyle = StyleUtil.buildHeadCellStyle(workbook, contentWriteCellStyle);
            //设置当前行第i列的样式
            cell.getRow().getCell(i).setCellStyle(cellStyle);
        }
    }
}
