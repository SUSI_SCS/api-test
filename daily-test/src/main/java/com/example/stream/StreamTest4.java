package com.example.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author 沈传尚
 * @date 2022/1/7
 */
public class StreamTest4 {
    public static void main(String[] args) {

        List<Integer> list = Arrays.asList(10, 5, 3, 10,2,3);
        // distinct() 去除相同的数据
        List<Integer> collect = list.stream().distinct().collect(Collectors.toList());
        collect.forEach(System.out::println);

        //skip() 跳过前n条数据
        List<Integer> collect1 = list.stream().skip(2).collect(Collectors.toList());
        System.out.println(collect1);

        //limit() 返回前n条数据
        List<Integer> collect2 = list.stream().limit(1).collect(Collectors.toList());
        System.out.println(collect2);





    }
}
