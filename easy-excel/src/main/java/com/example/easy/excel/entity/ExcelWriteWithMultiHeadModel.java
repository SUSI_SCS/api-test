package com.example.easy.excel.entity;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author 沈传尚
 * @date 2022/2/19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class ExcelWriteWithMultiHeadModel extends BaseRowModel {
    @ExcelProperty(value = {"表头p1","表头p11"})
    private String p1;

    @ExcelProperty(value = {"表头p2","表头p22"})
    private String p2;

    @ExcelProperty(value = {"表头p3","表头p33"})
    private String p3;
}
