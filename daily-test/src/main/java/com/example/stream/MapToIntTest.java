package com.example.stream;

import java.util.Arrays;
import java.util.List;

/**
 * @author 沈传尚
 * @date 2022/1/25
 */
public class MapToIntTest {
    public static void main(String[] args) {
        /*
        mapToInt(T -> int) 、mapToDouble(T -> double) 、mapToLong(T -> long)
        int sumVal = userList.stream().map(User::getAge).reduce(0,Integer::sum)；计算元素总和的方法其中暗含了装箱成本，
        map(User::getAge) 方法过后流变成了 Stream 类型，而每个 Integer 都要拆箱成一个原始类型再进行 sum 方法求和，这样大大影响了效率。
        针对这个问题 Java 8 有良心地引入了数值流 IntStream, DoubleStream, LongStream，这种流中的元素都是原始数据类型，
        分别是 int，double，long。
         */
        List<User> list= Arrays.asList(User.builder().age(18).build(),User.builder().age(45).build(),User.builder().age(15).build(),User.builder().age(5).build());
        //

        long start = System.currentTimeMillis();
//        Integer sum = list.stream().map(User::getAge).reduce(0,Integer::sum);
        int sum = list.stream().mapToInt(User::getAge).sum();     // 免去了装箱成本 提升效率 20ms 左右
        long end = System.currentTimeMillis();
        System.out.println("耗时:"+(end-start));


        double min = list.stream().mapToDouble(User::getAge).min().getAsDouble();
        double avg = list.stream().mapToInt(User::getAge).average().getAsDouble();
        System.out.println("min:"+min);
        System.out.println("avg:"+avg);


    }
}
