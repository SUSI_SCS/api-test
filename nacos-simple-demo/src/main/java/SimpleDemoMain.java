import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;

import java.util.Properties;

/**
 * @author 沈传尚
 * @date 2022/1/28
 */
public class SimpleDemoMain {
    public static void main(String[] args) throws NacosException {
        // nacos地址
        String serverAddr="127.0.0.1:8848";
        // Data Id
        String dataId="nacos-simple-demo.yaml";
        // Group
        String group="DEFAULT_GROUP";
        Properties properties=new Properties();
        properties.put("serverAddr",serverAddr);
        ConfigService configService = NacosFactory.createConfigService(properties);
        //获取配置
        String content = configService.getConfig(dataId, group, 5000);
        System.out.println(content);

    }
}
