package com.bz.fastjson;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 沈传尚
 * @date 2022/1/24
 */
public class Test2 {
    private static final Map<String,String> userSortMap = new HashMap<>();

    static {
        userSortMap.put("nickname","nickname");
        userSortMap.put("deptFullName","dept_full_name");
        userSortMap.put("positionName","position_name");
        userSortMap.put("rankName","option_name");
        userSortMap.put("sex","sex");
        userSortMap.put("roleName","tb_role_name");
        userSortMap.put("entryDate","entry_date");
        userSortMap.put("leaveDate","leave_date");
        userSortMap.put("userStatusName","user_status");
        userSortMap.put("leaveReason","leave_reason");
    }

    public static Map<String, String> getUsersortmap() {
        return userSortMap;
    }

}
