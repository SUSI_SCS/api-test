package com.example.page.help.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.page.help.entity.User;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 沈传尚
 * @since 2022-01-22
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}
