package com.example.map;

import java.util.HashMap;
import java.util.Map;

/**
 * @author susi
 * @date 2022/5/24
 */
public class PutIfAbsent {
    public static void main(String[] args) {
        Map<String,Integer> map=new HashMap<>();
        map.put("数学",100);
        map.putIfAbsent("数学",90);

        Integer score = map.get("数学");   // 100

        System.out.println(score);


    }
}
