package com.example.scheduled.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author 沈传尚
 * @date 2022/1/27
 */
@Component
public class PrintTask {

//    @Scheduled(cron = "5 * * * * *")   // 每分钟的第5s开始执行
//    public void corn(){
//        System.out.println(Thread.currentThread().getName()+"corn执行时间:"+new Date(System.currentTimeMillis()));
//    }

//    @Scheduled(cron = "0 7 17 * * ?")    // 每5s执行一次
//    public void corn1(){
//        System.out.println(Thread.currentThread().getName()+"corn1执行时间:"+new Date(System.currentTimeMillis()));
//    }

//    @Scheduled(cron = "*/5 * * * * *")   //每5s执行一次
//    public void corn2(){
//        System.out.println(Thread.currentThread().getName()+"corn2执行时间:"+new Date(System.currentTimeMillis()));
//    }

//    每隔5秒执行一次：*/5 * * * * ?
//
//    每隔1分钟执行一次：0 */1 * * * ?
//
//    每天23点执行一次：0 0 23 * * ?
//
//    每天凌晨1点执行一次：0 0 1 * * ?
//
//    每月1号凌晨1点执行一次：0 0 1 1 * ?
//
//    每月最后一天23点执行一次：0 0 23 L * ?
//
//    每周星期天凌晨1点实行一次：0 0 1 ? * L
//
//    在26分、29分、33分执行一次：0 26,29,33 * * * ?
//
//    每天的0点、13点、18点、21点都执行一次：0 0 0,13,18,21 * * ?

//    每天每隔一小时执行一次 0 * */2 * * ?

    @Scheduled(cron = "0 */1 * * * ?")   //每1分钟执行一次
    public void corn2(){
        System.out.println(Thread.currentThread().getName()+"corn2执行时间:"+new Date(System.currentTimeMillis()));
    }

}
