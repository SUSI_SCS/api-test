package com.example.stream;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 沈传尚
 * @date 2021/12/23
 */
@Data
@Builder
public class User {
    private String idCard;
    private String name;
    private List<String> list;
    private Integer age;
    private String deptName;
    private BigDecimal salary;
}
