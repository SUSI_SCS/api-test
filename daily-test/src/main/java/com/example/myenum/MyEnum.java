package com.example.myenum;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @author 沈传尚
 * @date 2022/1/7
 */
@AllArgsConstructor
@NoArgsConstructor
public enum MyEnum {
    STATUS(200,"OK");
    private int code;
    private String msg;
}
