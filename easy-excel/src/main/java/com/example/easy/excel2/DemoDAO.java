//package com.example.easy.excel2;
//
///**
// * @author 沈传尚
// * @date 2022/3/14
// */
//
//import com.alibaba.excel.EasyExcel;
//import com.alibaba.excel.ExcelWriter;
//import com.alibaba.excel.write.metadata.WriteSheet;
//import com.alibaba.excel.write.metadata.fill.FillConfig;
//import com.alibaba.excel.write.metadata.style.WriteCellStyle;
//import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
//
//import java.util.List;
//
///**
// * 假设这个是你的DAO存储。当然还要这个类让spring管理，当然你不用需要存储，也不需要这个类。
// **/
//public class DemoDAO {
//    //    public void save(List<DemoData> list) {
////        // 如果是mybatis,尽量别直接调用多次insert,自己写一个mapper里面新增一个方法batchInsert,所有数据一次性插入
////    }
//// 内容的策略
//    WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
//// 这里需要指定 FillPatternType 为FillPatternType.SOLID_FOREGROUND 不然无法显示背景颜色.头默认了 FillPatternType所以可以不指定
//contentWriteCellStyle.setWrapped(true);
//    // 这个策略是 头是头的样式 内容是内容的样式 其他的策略可以自己实现
//    HorizontalCellStyleStrategy horizontalCellStyleStrategy =
//            new HorizontalCellStyleStrategy(null, contentWriteCellStyle);
//
//    ExcelWriter excelWriter = EasyExcel.write(fileName).registerWriteHandler(horizontalCellStyleStrategy).withTemplate(templateFileName).build();
//    WriteSheet writeSheet = EasyExcel.writerSheet().build();
//    // 这里注意 入参用了forceNewRow 代表在写入list的时候不管list下面有没有空行 都会创建一行，然后下面的数据往后移动。默认 是false，会直接使用下一行，如果没有则创建。
//// forceNewRow 如果设置了true,有个缺点 就是他会把所有的数据都放到内存了，所以慎用
//// 简单的说 如果你的模板有list,且list不是最后一行，下面还有数据需要填充 就必须设置 forceNewRow=true 但是这个就会把所有数据放到内存 会很耗内存
//// 如果数据量大 list不是最后一行 参照下一个
//    FillConfig fillConfig = FillConfig.builder().forceNewRow(Boolean.TRUE).build();
//}
