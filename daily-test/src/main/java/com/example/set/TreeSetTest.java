package com.example.set;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author 沈传尚
 * @date 2021/12/27
 */
/*
TreeSet 是一个有序的集合，它的作用是提供有序的Set集合。
 */
public class TreeSetTest {
    public static void main(String[] args) {
        Set<Integer> treeSet=new TreeSet<>();
        treeSet.add(5);
        treeSet.add(10);
        treeSet.add(1);
        treeSet.forEach(System.out::println);
    }
}
