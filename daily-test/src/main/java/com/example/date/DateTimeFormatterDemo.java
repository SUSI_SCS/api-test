package com.example.date;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author 沈传尚
 * @date 2022/4/1
 */
public class DateTimeFormatterDemo {
    public static void main(String[] args) {
        String date = DateTimeFormatter.ofPattern("yyyy/MM/dd").format(LocalDate.now());
        System.out.println(date);

    }
}
