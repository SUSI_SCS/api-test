package com.example.list;

import java.util.Arrays;
import java.util.List;

/**
 * @author susi
 * @date 2022/6/13
 */
public class ListDemo {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("苹果","香蕉","菠萝");

        String s = list.toString().replaceAll(", ",";");
        System.out.println(s);

        System.out.println(s.substring(s.indexOf(";")+1,s.lastIndexOf("]")));
    }
}
