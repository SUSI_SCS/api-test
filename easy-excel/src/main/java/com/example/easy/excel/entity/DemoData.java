package com.example.easy.excel.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author 沈传尚
 * @date 2022/2/19
 */
@Data
public class DemoData {
    /**
     * 强制读取第三个 这里不建议 index 和 name 同时用，要么一个对象只用index，要么一个对象只用name去匹配
     */
    /** 数值标题 **/
    @ExcelProperty(index = 2)
    private Double doubleData;
    /**
     * 用名字去匹配，这里需要注意，如果名字重复，会导致只有一个字段读取到数据
     */
    @ExcelProperty("字符串标题")
    private String string;

    /** 将从excel读取到的日期标题转为对应的时间戳(毫秒数 1645200000000=2022-02-19 00:00:00) **/
    @ExcelProperty("日期标题")
    private String date;
}
