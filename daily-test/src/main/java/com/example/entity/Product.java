package com.example.entity;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.TreeSet;

/**
 * @author 沈传尚
 * @date 2021/12/27
 */
@Data
@Builder
public class Product {
    private Integer id;
    private Integer num;
    private BigDecimal price;
    private String name;
    private String category;
    private TreeSet<Product> children;


}
