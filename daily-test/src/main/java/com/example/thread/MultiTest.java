package com.example.thread;


/**
 * @author 沈传尚
 * @date 2021/12/24
 */
public class MultiTest {
    public static void main(String[] args) {
        MutilateThreadService<Object> thread=new MutilateThreadService<>(10,10);
        thread.setCallable(()->{
            System.out.println("这是第一条线程");
            return null;
        });

        thread.setCallable(()->{
            System.out.println("这是第二条线程");
            return null;
        });

        thread.setCallable(()->{
            System.out.println("这是第三条线程");
            return null;
        });

        thread.exec();


    }
}
