package com.example.string;

/**
 * @author 沈传尚
 * @date 2022/3/2
 */
public class RemoveZeroFirst {
    public static void main(String[] args) {
        String s="00000123";
        System.out.println(method2(s));


    }

    public static String method1(String s){
        return  Integer.parseInt(s)+"";
    }

    public static String method2(String s){
        return  s.replaceAll("^0*","");
    }

    public static String method3(String s){
        return s.replaceFirst("^(0+)*", "");
    }
}
