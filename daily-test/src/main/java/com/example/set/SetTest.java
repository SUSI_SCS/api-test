package com.example.set;

import java.util.HashSet;
import java.util.Set;

/**
 * @author 沈传尚
 * @date 2021/12/23
 */
public class SetTest {
    public static void main(String[] args) {
        /**HashSet是一个没有重复元素的集合，它其实是由HashMap实现的，HashMap保存的是建值对，然而我们只能向HashSet中添加Key，
         * 原因在于HashSet的Value其实都是同一个对象，这是HashSet添加元素的方法，可以看到辅助实现HashSet的map中的value其实都是Object类的同一个对象。
         *
         * set集合:
         * 不存储重复的元素 无序
         */
        Set<Integer> set=new HashSet<>();
        set.add(5);
        set.add(4);
        set.add(2);
        set.add(8);
        set.add(10);
        for (Integer s :set){
            System.out.println(s);
        }
    }
}
