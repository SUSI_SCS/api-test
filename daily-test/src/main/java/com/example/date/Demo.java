package com.example.date;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 沈传尚
 * @date 2022/2/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Demo {
    private String dept;
}
