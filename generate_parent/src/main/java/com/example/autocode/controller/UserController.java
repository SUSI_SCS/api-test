package com.example.autocode.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 沈传尚
 * @since 2022-01-22
 */
@Controller
@RequestMapping("/autocode/user")
public class UserController {

}

