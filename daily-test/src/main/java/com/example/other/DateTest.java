package com.example.other;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author 沈传尚
 * @date 2022/2/10
 */
public class DateTest {
    public static void main(String[] args) {
        String s = dayComparePrecise("2022-02-15", "2023-05-10");
        System.out.println(s);
    }

    public static String dayComparePrecise(String fromDate, String toDate){

        Period period = Period.between(LocalDate.parse(fromDate), LocalDate.parse(toDate));

        StringBuffer sb = new StringBuffer();
        sb.append(period.getYears()).append("年")
                .append(period.getMonths()).append("月").append(period.getDays()).append("日");
        return sb.toString();
    }
}
