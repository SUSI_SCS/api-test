package com.example.recursive;

/**
 * @author 沈传尚
 * @date 2022/1/5
 */
public class Test1 {
    public static void main(String[] args) {
        System.out.println(sum(5));
    }
    public static int sum(int num){
        if (num == 1){
            return 1;
        }else {
            return num+sum(num-1);
        }
    }
}
