package com.example.easy.excel.excelwriteold;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.example.easy.excel.entity.ExcelPropertyIndexModel;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 沈传尚
 * @date 2022/2/19
 */
public class ExcelWriteWithEntityHeadTest {
    public static void main(String[] args) {
        excelWriteWithEntityHead();
    }

    /**
     * 单行表头(自定义实体类)
     */
    public static void excelWriteWithEntityHead(){
        try {
            OutputStream outputStream=new FileOutputStream("withEntityHead.xlsx");
            ExcelWriter excelWriter=new ExcelWriter(outputStream, ExcelTypeEnum.XLSX);
            Sheet sheet=new Sheet(1,1, ExcelPropertyIndexModel.class);
            sheet.setSheetName("sheet1");
            List<ExcelPropertyIndexModel> list=new ArrayList<>();
            for (int i = 0; i < 50; i++) {
                list.add(ExcelPropertyIndexModel.builder().name("name"+i).age("age"+i)
                        .address("address"+i).height("height"+i).sex("sex"+i).email("email"+i).build()
                );
            }
            excelWriter.write0(list,sheet);
            excelWriter.finish();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println(e.getMessage().toUpperCase());
        }
    }
}
