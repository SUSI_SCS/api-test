package com.example.stream;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

/**
 * @author 沈传尚
 * @date 2022/2/18
 */
public class Test {
    public static void main(String[] args) {
        System.out.println(removeZero("10年10月10日"));

    }

    private static String removeZero(String  s){
        //        显示年月日
        if (s.charAt(0) != '0' && s.charAt(s.indexOf("年")+1) != '0' && s.charAt(s.indexOf("月")+1) != '0'){
            return s;
        }
        // 年0    显示月日
        if (s.charAt(0) == '0' && s.charAt(s.indexOf("年")+1) != '0' && s.charAt(s.indexOf("月")+1) != '0'){
           return s.substring(s.indexOf("年")+1);
        }
        // 月0    显示年日
        if (s.charAt(0) != '0' && s.charAt(s.indexOf("年")+1) == '0' && s.charAt(s.indexOf("月")+1) != '0'){
            return s.substring(0,s.indexOf("年")+1)+s.substring(s.indexOf("月")+1);
        }
        // 日0    显示年月
        if (s.charAt(0) != '0' && s.charAt(s.indexOf("年")+1) != '0' && s.charAt(s.indexOf("月")+1) == '0'){
            return s.substring(0,s.indexOf("月")+1);
        }
        // 年0 月0  显示日
        if (s.charAt(0) == '0' && s.charAt(s.indexOf("年")+1) == '0' && s.charAt(s.indexOf("月")+1) != '0'){
            return s.substring(s.indexOf("月")+1);
        }
        // 年0 日0 显示月
        if (s.charAt(0) == '0' && s.charAt(s.indexOf("年")+1) != '0' && s.charAt(s.indexOf("月")+1) == '0'){
            return s.substring(s.indexOf("年")+1,s.indexOf("月")+1);
        }
        // 月0 日0  显示年
        if (s.charAt(0) != '0' && s.charAt(s.indexOf("年")+1) == '0' && s.charAt(s.indexOf("月")+1) == '0'){
            return s.substring(0,s.indexOf("年")+1);
        }
        // 年0 月0  日0   不显示
        if (s.charAt(0) == '0' && s.charAt(s.indexOf("年")+1) == '0' && s.charAt(s.indexOf("月")+1) == '0'){
            return "";
        }
        return "";
    }
}
