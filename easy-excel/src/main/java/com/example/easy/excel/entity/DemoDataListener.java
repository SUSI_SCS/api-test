package com.example.easy.excel.entity;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 沈传尚
 * @date 2022/2/19
 */
public class DemoDataListener extends AnalysisEventListener<DemoData> {
    List<DemoData> list=new ArrayList<>();

    public DemoDataListener(){

    }

    /**
     * 每一条数据解析都会调用该方法
     * @param data
     * @param context
     */
    @Override
    public void invoke(DemoData data, AnalysisContext context) {
        System.out.println("解析到一条数据:"+JSON.toJSONString(data));
        list.add(data);
    }

    /**
     * 所有数据都解析完了 都会来调用
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        System.out.println("已全部解析完毕:"+JSON.toJSONString(list));

    }

    // 获取list
    public  List<DemoData> getData(){
        return list;
    }
}
