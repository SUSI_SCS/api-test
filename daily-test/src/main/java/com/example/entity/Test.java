package com.example.entity;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 沈传尚
 * @date 2022/2/17
 */
public class Test {
    public static void main(String[] args) {
        List<Point> list=new ArrayList<>();
        list.add(Point.builder().x(5).y(null).build());

        Map<Integer, Integer> collect = list.stream().collect(Collectors.toMap(Point::getX, Point::getY));
        System.out.println(collect);
    }
}
