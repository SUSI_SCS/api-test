package com.example.easy.excel.excelwriteold;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.metadata.Table;
import com.alibaba.excel.support.ExcelTypeEnum;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 沈传尚
 * @date 2022/2/19
 */
public class ExcelWriteWithHeadTest {
    public static void main(String[] args) {
        writeWithHead();
    }

    /**
     * 单行表头()
     */
    public static void writeWithHead(){
        try(OutputStream out=new FileOutputStream("withHead.xlsx");){
            ExcelWriter writer=new ExcelWriter(out, ExcelTypeEnum.XLSX);
            Sheet sheet1=new Sheet(1,0); // 指定sheet页 从1开始; 指定开始行 从0开始
            sheet1.setSheetName("sheet2");
            List<List<String>> data=new ArrayList<>();
            for (int i = 0; i < 100; i++) {
                List<String> item=new ArrayList<>();
                item.add("item"+i);
                item.add("item"+i);
                item.add("item"+i);
                data.add(item);
            }
            List<List<String>> head=new ArrayList<>();
            List<String> headColumn1=new ArrayList<>();
            List<String> headColumn2=new ArrayList<>();
            List<String> headColumn3=new ArrayList<>();
            headColumn1.add("第一列");
            headColumn2.add("第二列");
            headColumn3.add("第三列");
            head.add(headColumn1);
            head.add(headColumn2);
            head.add(headColumn3);
            Table table=new Table(1);
            table.setHead(head);
            writer.write0(data,sheet1,table);
            writer.finish();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
}
