package com.example.iterable.collection;


import org.springframework.util.CollectionUtils;

import java.util.ArrayList;

/**
 * @author 沈传尚
 * @date 2021/12/23
 */
public class IsEmptyTest {
    public static void main(String[] args) {
        /*判断集合是否为空*/
        boolean result = CollectionUtils.isEmpty(new ArrayList<>());
        System.out.println(result);


    }
}
