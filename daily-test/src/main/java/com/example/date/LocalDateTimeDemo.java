package com.example.date;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author 沈传尚
 * @date 2022/4/24
 */
public class LocalDateTimeDemo {
    public static void main(String[] args) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime parse1 = LocalDateTime.parse("2022/04/24 11:08:30",dtf);
        LocalDateTime parse2 = LocalDateTime.parse("2022/04/24 12:08:30",dtf);
        Duration between = Duration.between(parse1, parse2);
        long seconds = between.getSeconds();
        String s = secToTime((int) seconds);

        System.out.println(s);

//        System.out.println(seconds);
    }


    public static String secToTime(int seconds) {
        int hour = seconds / 3600;
        int minute = (seconds - hour * 3600) / 60;
        int second = (seconds - hour * 3600 - minute * 60);

        StringBuffer sb = new StringBuffer();
        if (hour > 0) {
            sb.append(hour + "时");
        }
        if (minute >= 0) {
            sb.append(minute + "分");
        }
        if (second >= 0) {
            sb.append(second + "秒");
        }

        return sb.toString();
    }

}
