package com.example.list;

import com.example.entity.Tag;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author 沈传尚
 * @date 2022/3/4
 */
public class CopyOnWriteArrayListTest {
    public static void main(String[] args) {
        List<Tag> tagList=new CopyOnWriteArrayList<>();
        List<Tag> tags=new ArrayList<>();
        tags.add(Tag.builder().name("标签1").build());
        tags.add(Tag.builder().name("标签2").build());
        tags.add(Tag.builder().name("标签3").build());
        tags.add(Tag.builder().name("标签4").build());
        tags.add(Tag.builder().name("标签5").build());
        tags.add(Tag.builder().name("标签6").build());

    }
}
