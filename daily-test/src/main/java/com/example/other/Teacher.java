package com.example.other;

import lombok.Data;

/**
 * @author 沈传尚
 * @date 2021/12/17
 */
@Data
public class Teacher {
    private Integer id=1;
    private String userName="小黑";
    private Integer age;
}
