package com.example.string;

import cn.hutool.core.util.StrUtil;
import com.example.entity.Person;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @author susi
 * @date 2022/7/7
 */
public class StrUtilDemo {
    public static void main(String[] args) {
            List<Person> personList = new ArrayList<Person>();
            personList.add(new Person("鸣人", 8900, 18, "1", "螺旋丸"));
            personList.add(new Person("鸣人", 8900, 18, "1", "螺旋丸"));
            personList.add(new Person("小樱", 7800, 17, "2", "治疗术"));
            personList.add(new Person("小樱", 7800, 17, "2", "治疗术"));
            personList.add(new Person("大蛇丸", 9500, 31, "1", "八岐大蛇"));
            personList.add(new Person("佐助", 8900, 18, "2", "须佐能乎"));
            personList.add(new Person("纲手", 7900, 29, "2", "百豪之术"));

            // list 一个条件 去重
            List<Person> uniqueByName = personList.stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(Person::getName))), ArrayList::new));
            System.out.println("一个条件去重后的list：" + uniqueByName);

            // list 多个条件 去重
            List<Person> uniqueBySalaryAndAge = personList.stream().collect(
                    Collectors.collectingAndThen(
                            Collectors.toCollection(() -> new TreeSet<>(
                                    Comparator.comparing(p -> p.getSalary() + ";" + p.getAge()))), ArrayList::new));
            System.out.println("多个条件去重后的list：" + uniqueBySalaryAndAge);
        }

}
