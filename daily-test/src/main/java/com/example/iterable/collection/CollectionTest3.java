package com.example.iterable.collection;



import com.example.entity.Point;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 沈传尚
 * @date 2021/12/24
 */
public class CollectionTest3 {
    public static void main(String[] args) {
        List<Point> list=new ArrayList<>();
        Point point = Point.builder().x(10).y(5).build();
        point.setX(10);
        point.setY(5);
        // 集合只能存放引用类型元素,并且存放的是元素的引用(地址)
        list.add(point);

        System.out.println(list);

        System.out.println(point);
    }
}
