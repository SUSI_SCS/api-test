package com.example.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 沈传尚
 * @date 2022/1/25
 */
public class AveragingTest {
    public static void main(String[] args) {
        List<User> list= Arrays.asList(
                User.builder().deptName("开发部").age(20).build(),
                User.builder().deptName("测试部").age(18).build(),
                User.builder().deptName("开发部").age(32).build(),
                User.builder().deptName("开发部").age(25).build()
        );

        // averagingInt()、averagingLong()、averagingDouble() 用于计算平均值
        Double avg = list.stream().collect(Collectors.averagingInt(User::getAge));
        System.out.println(avg);

    }
}
