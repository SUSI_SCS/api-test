package com.example.page.help.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.page.help.entity.User;
import com.example.page.help.mapper.UserMapper;
import com.example.page.help.service.UserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 沈传尚
 * @since 2022-01-22
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
