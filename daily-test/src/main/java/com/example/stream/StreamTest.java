package com.example.stream;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

/**
 * @author 沈传尚
 * @date 2021/12/13
 */
public class StreamTest {
    public static void main(String[] args) {
        /**
         * 在 Java 8 中, 集合接口有两个方法来生成流：
         * (1): stream() − 为集合创建串行流。
         * (2): parallelStream() − 为集合创建并行流。
         */
        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abed", "", "jkl");
        List<String> filtered = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
        System.out.println(filtered);

        List<Integer> vector=new Vector<>();
        vector.add(1);
        System.out.println(vector);

    }
}
