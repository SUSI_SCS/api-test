package com.example.date;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author 沈传尚
 * @date 2022/4/24
 */
public class LocalDateTimeParseDemo {
    public static void main(String[] args) {
        DateTimeFormatter dtf=DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss");

        dtf.parse(LocalDateTime.now().toString());

    }
}
