package com.example.list;

import java.util.*;

/**
 * @author 沈传尚
 * @date 2022/4/8
 */
public class AsListDemo {
    public static void main(String[] args) {

//        List<String> list = Arrays.asList("5", "10");
//        String s = listToString(list);
//        System.out.println(s);

        List<Integer> list1 = stringToList("10,20,30");
        System.out.println(list1);

    }

    private static String listToString(List<String> list){
        if (Objects.nonNull(list) && list.size() >0){
            StringBuilder builder=new StringBuilder();
            for (String s : list){
                builder.append(s).append(",");
            }
            return builder.deleteCharAt(builder.lastIndexOf(",")).toString();
        }
        return "";
    }

    /**
     * 将字符串拆开 转成集合.
     */
    private static List<Integer> stringToList(String s){
        if (Objects.nonNull(s)){
            List<Integer> list=new ArrayList<>();
            if (!s.contains(",")){
                list.add(Integer.parseInt(s));
            }else {
                String[] splits = s.split(",");
                for (String split : splits){
                    list.add(Integer.parseInt(split));
                }
            }
            return list;
        }
        return new ArrayList<>();
    }
}
