package com.example.common;

/**
 * @author 沈传尚
 * @date 2022/3/9
 */
public enum ResultCodeEnum {
    SUCCESSFUL(10000, "访问成功"),
    FAIL(500, "系统内部错误");

    private int code;
    private String msg;

    private ResultCodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }
}
