package com.example.iterable.collection.list;

/**
 * @author 沈传尚
 * @date 2022/1/8
 */
public class LinkedListTest {
    public static void main(String[] args) {

        Integer[] arr=new Integer[]{1,2,3};
        caseTest(arr);
    }

    public static  void caseTest(Integer[] array){
        for (int i = 0; i < array.length; i++) {
            switch (i){
                case 0:
                    System.out.println("0");
                    continue;
                case 1:
                    System.out.println("1");
                    continue;
                case 2:
                    System.out.println("2");
            }

        }

    }
}
