package com.example.easy.excel.excelreadnew;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.example.easy.excel.entity.DemoData;
import com.example.easy.excel.entity.DemoDataListener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.List;

/**
 * @author 沈传尚
 * @date 2022/2/19
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class EasyReadTest {

    @Test
    public void readTest(){
        try {
            String filePath="./../demo.xlsx";
            EasyExcel.read(filePath,DemoData.class,new DemoDataListener()).sheet().doRead();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void readTest2(){
        String filePath="./../demo.xlsx";
        DemoDataListener listener=new DemoDataListener();
        ExcelReader excelReader = EasyExcel.read(filePath, DemoData.class, listener).build();
        ReadSheet readSheet=EasyExcel.readSheet(0).build();
        excelReader.read(readSheet);
        // 千万别忘记关闭 读的时候会创建临时文件 磁盘会崩的
        excelReader.finish();
        List<DemoData> data = listener.getData();
        // 处理日期 2022-02-19 00:00:00  -> 2022-02-19
        for (DemoData d : data){
            d.setDate(d.getDate().substring(0, d.getDate().indexOf(" ")));
        }

//        SimpleDateFormat format=new SimpleDateFormat("yyyy/MM/dd");
        for (DemoData demo : data){
            String dateTitle = demo.getDate();
//            String dateTitle = format.format(date);
            String string = demo.getString();
            Double doubleData = demo.getDoubleData();
            System.out.println("字符串标题:"+string+"\t"+"日期标题:"+dateTitle+"\t"+"数值标题:"+doubleData);
        }

    }


}
