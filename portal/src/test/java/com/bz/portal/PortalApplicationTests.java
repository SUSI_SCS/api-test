package com.bz.portal;

import com.bz.portal.entity.SalaryModel;
import com.bz.portal.mapper.SalaryModelMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootTest
class PortalApplicationTests {
    @Autowired(required = false)
    private SalaryModelMapper salaryModelMapper;

    @Test
    public void findByIdTest(){
        SalaryModel salaryModel = salaryModelMapper.selectById(1);
        System.out.println(salaryModel);
    }

    @Scheduled(cron = "5 0 0 * * *?")
    @org.junit.Test
    public void scheduledTest(){
        System.out.println("5s");
    }

}
