package com.example.object.oriented;

/**
 * @author 沈传尚
 * @date 2022/4/6
 */
public class Demo1 {
    public Student getStudent(){
        return new Student();
    }

}

class Student{
    public static int a=5;
    int b=7;


    public static  void  get(){
        //
        System.out.println(a);
    };

    public Student test(){
        return this;
    }

    public Student getStudent(){
        return new Student();
    }

    public  boolean isSame(){
      return   test() == getStudent();
    }

    public static void main(String[] args) {
        Student s = new Student();
        boolean same = s.isSame();
        System.out.println(same);
    }
}
