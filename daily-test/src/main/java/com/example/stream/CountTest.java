package com.example.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 沈传尚
 * @date 2022/1/25
 */
public class CountTest {
    public static void main(String[] args) {
        //使用 counting() 和 count() 可以对列表数据进行统计。

        List<User> list= Arrays.asList(
                User.builder().deptName("开发部").age(20).build(),
                User.builder().deptName("测试部").age(18).build(),
                User.builder().deptName("开发部").age(32).build(),
                User.builder().deptName("开发部").age(25).build()
        );
        Long count = list.stream().filter(user -> user.getDeptName().equals("开发部")).collect(Collectors.counting());

        long count1 = list.stream().filter(user -> user.getAge() > 30).count();
        System.out.println(count);
        System.out.println(count1);
    }
}
