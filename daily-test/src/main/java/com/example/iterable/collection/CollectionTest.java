package com.example.iterable.collection;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 沈传尚
 * @date 2021/12/23
 */
/*
 * java集合框架
 * java.util.Collection接口,所有集合的顶级接口,里面规定了集合所必须
 * 具备的相关功能.
 * 集合与数组一样,可以保存一组元素,并且对于元素的操作都封装成了方法.
 *
 * 集合有多种实现类,实现了不同的数据结构.实际开发中我们可以结合实际需求
 * 选取合适的数据结构来存储元素.
 *
 * Collection下面有两个常见的子接口:
 * java.util.List:线性表,特点:可以存放重复元素,并且有序.
 * java.util.Set:不可重复集.即:不能存放重复元素.
 * 元素是否重复是依靠元素自身的equals比较来判定的,即:Set集合不能存在
 * 两个元素equals比较为true的情况.
 */
public class CollectionTest {
    public static void main(String[] args) {
        List<Integer> list=new ArrayList<>();
        list.add(5);
        list.add(8);
        list.add(10);
        // 判断集合是否为空
        // list.isEmpty();
        // 清空集合
        System.out.println(list.size());
        list.clear();
        System.out.println(list);
    }
}
