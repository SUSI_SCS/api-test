package com.example.stream;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author 沈传尚
 * @date 2022/1/24
 */
public class ReduceTest {
    public static void main(String[] args) {
        // 使用 reduce((T, T) -> T) 和 reduce(T, (T, T) -> T) 用于组合流中的元素，如求和，求积，求最大值等。
        BigDecimal b1=new BigDecimal("5.0");
        BigDecimal b2=new BigDecimal("5.23");
        BigDecimal b3=new BigDecimal("4.85");
        List<BigDecimal> list= Arrays.asList(b1,b2,b3);

        double avg = list.stream().mapToDouble(BigDecimal::doubleValue).average().getAsDouble();
        BigDecimal b=new BigDecimal(avg).setScale(1,BigDecimal.ROUND_HALF_UP);

        System.out.println(b);

        BigDecimal bigDecimal = list.stream().reduce(BigDecimal::add).get();
        System.out.println(bigDecimal);


    }
}
