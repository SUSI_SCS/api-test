package com.example.string;

/**
 * @author 沈传尚
 * @date 2022/3/12
 */
public class SubTest {
    public static void main(String[] args) {
        String s="/hr/optionExit/updateOptionStatus/1235";
        String substring = s.substring(0, s.lastIndexOf("/") + 1);
        boolean result = substring.equals("/hr/optionExit/updateOptionStatus/");
        System.out.println(result);

    }
}
