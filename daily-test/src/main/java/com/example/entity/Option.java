package com.example.entity;

import lombok.Data;

/**
 * @author 沈传尚
 * @date 2022/3/10
 */
@Data
public class Option {
    private Integer approvalId;

    private Integer approvalLevel;
}
