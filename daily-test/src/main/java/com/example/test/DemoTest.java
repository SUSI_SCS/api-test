package com.example.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 沈传尚
 * @date 2022/4/1
 */
public class DemoTest {
    public static void main(String[] args) {
        List<String> list = Arrays.asList(null);
    }


    public static <T> List<T> removeNull(List<? extends T> oldList) {
        List<T> listTemp = new ArrayList();
        for (T t : oldList) {
            if (t != null) {
                listTemp.add(t);
            }
        }
        return listTemp;
    }
}
