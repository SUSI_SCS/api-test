package com.example.stream;



import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 文档注释只在三个地方使用,分别是类上,方法上和常量上.
 * 在类上使用时用来说明当前类的设计意图,功能等介绍.
 * @author 沈传尚
 * @date 2021/12/23
 */
public class StreamTest2 {

    /**
     *
     */
    private final static String INFO="APPROVAL";


    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        /*
         * java8 stream().map().collect()用法
         * 场景分析: 要获取所有用户的身份证号 并存放在以及集合中
         */
        // 1.有如下用户 存储在了list集合中
        User tom = User.builder().idCard("655351").name("tom").build();
        User rose = User.builder().idCard("655352").name("rose").build();
        User jack = User.builder().idCard("655353").name("jack").build();
        List<User> users=new ArrayList<>();
        users.add(tom);
        users.add(rose);
        users.add(jack);

        // 2.获取Users对象中每个用户的身份证号 传统做法
        List<String> list=new ArrayList<>();
        for (User user : users){
            list.add(user.getIdCard());
        }

        // 3.采用stream().map().collect()用法
        List<User> collect = users.stream().map(user -> {
            User u = User.builder().idCard(user.getIdCard()).build();
            return u;   //
        }).collect(Collectors.toList());
        System.out.println(collect);

    }

}
