package com.example.entity;

import lombok.Data;

import java.util.List;

/**
 * @author 沈传尚
 * @date 2022/4/18
 */
@Data
public class Resource {

    private List<Integer> examinerIds;
}
