package com.example.entity;

import lombok.Builder;
import lombok.Data;

/**
 * @author 沈传尚
 * @date 2022/3/4
 */
@Data
@Builder
public class Tag {
    private String name;
}
