package com.example.entity;

/**
 * @author 沈传尚
 * @date 2022/3/23
 */
public class Person {
    private Integer x;
    private Integer y;

    public Person(){};
    public Person(Integer x){
        this.x=x;
    }
    public Person(Integer x, Integer y){
        this.x=x;
        this.y=y;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "(X="+x+ ","+ "y="+ y+")";
    }
}
