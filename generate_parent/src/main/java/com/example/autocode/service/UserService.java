package com.example.autocode.service;

import com.example.autocode.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 沈传尚
 * @since 2022-01-22
 */
public interface UserService extends IService<User> {

}
