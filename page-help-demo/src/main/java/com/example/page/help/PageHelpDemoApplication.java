package com.example.page.help;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan
public class PageHelpDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PageHelpDemoApplication.class, args);
    }

}
