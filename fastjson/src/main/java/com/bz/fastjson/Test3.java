package com.bz.fastjson;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author 沈传尚
 * @date 2022/1/24
 */
public class Test3 {
    public static void main(String[] args) {
        BigDecimal b1=new BigDecimal("1.20").stripTrailingZeros();
        BigDecimal b2=new BigDecimal("2.583").setScale(2,BigDecimal.ROUND_HALF_UP);
        System.out.println(b1);
        System.out.println(b2);
    }
}
