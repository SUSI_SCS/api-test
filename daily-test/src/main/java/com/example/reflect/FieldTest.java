package com.example.reflect;

import com.example.entity.PersonByOption;

import javax.validation.constraints.NotNull;
import java.lang.reflect.Field;
import java.util.Objects;

/*
(1) getFields()：获得某个类的所有的公共（public）的字段，包括父类中的字段。
(2)  getDeclaredFields()：获得某个类的所有声明的字段，即包括public、private和proteced，但是不包括父类的申明字段。
* */
public class FieldTest {
    public static void main(String[] args) {
        PersonByOption option=new PersonByOption();
        option.setAge(20);
        try {
            check(option);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void check(Object obj) throws Exception {
        Class<Object> clazz = Object.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field :fields){
            NotNull notNull = field.getAnnotation(NotNull.class);
            if (notNull!=null){
                Object o = field.get(obj);
                if (Objects.isNull(o)){
                    String message = notNull.message();
                    throw new Exception(message);
                }
            }
        }
    }

}
