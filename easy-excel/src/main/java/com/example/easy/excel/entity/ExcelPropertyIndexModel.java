package com.example.easy.excel.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author 沈传尚
 * @date 2022/2/19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class ExcelPropertyIndexModel extends BaseRowModel {
    @ExcelProperty(value = "姓名",index = 0)
    private String name;

    @ExcelProperty(value = "年龄",index = 1)
    private String age;

    @ExcelProperty(value = "邮箱", index = 2)
    private String email;

    @ExcelProperty(value = "地址", index = 3)
    private String address;

    @ExcelProperty(value = "性别",index = 4)
    private String sex;

    @ExcelProperty(value = "身高",index = 5)
    private String height;

}
