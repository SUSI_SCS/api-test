package com.example.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 沈传尚
 * @date 2022/3/9
 */
public class FilterDemo {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 5, 6);
        List<Integer> collect = list.stream().filter(u -> u.equals(1)).collect(Collectors.toList());
        System.out.println(collect);
        System.out.println(collect.size());
    }
}
