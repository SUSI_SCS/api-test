package com.example.easy.excel.excelwriteold;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.example.easy.excel.entity.ExcelWriteWithMultiHeadModel;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 沈传尚
 * @date 2022/2/19
 */
public class ExcelWriteWithMultiHeadTest {
    public static void main(String[] args) {
        excelWriteWithMultiHead();
    }

    /**
     * 多行表头
     */
    public static void excelWriteWithMultiHead(){
        try {
            OutputStream outputStream=new FileOutputStream("withMultiHead.xlsx");
            ExcelWriter excelWriter=new ExcelWriter(outputStream, ExcelTypeEnum.XLSX);
            Sheet sheet=new Sheet(1,0, ExcelWriteWithMultiHeadModel.class);
            List<ExcelWriteWithMultiHeadModel> list=new ArrayList<>();
            for (int i = 0; i < 30; i++) {
                list.add(ExcelWriteWithMultiHeadModel.builder().p1("p1"+i).p2("p2"+i).p3("p3"+i).build());
            }
            excelWriter.write0(list,sheet);
            excelWriter.finish();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
