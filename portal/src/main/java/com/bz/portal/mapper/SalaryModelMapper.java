package com.bz.portal.mapper;

import com.bz.portal.entity.SalaryModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 沈传尚
 * @since 2022-01-17
 */
public interface SalaryModelMapper extends BaseMapper<SalaryModel> {






}
