package com.bz.portal.service;

import com.bz.portal.entity.SalaryModel;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bz.portal.util.JsonResult;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 沈传尚
 * @since 2022-01-17
 */
public interface SalaryModelService extends IService<SalaryModel> {

    JsonResult exceptionTest();

    /**
     *
     * @param type 岗位类型 (0 职能岗 1 研发岗)
     * @return
     */
    List<SalaryModel> listAll(Integer type);

}
