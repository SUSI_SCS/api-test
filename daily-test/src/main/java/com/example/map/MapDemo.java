package com.example.map;

import com.example.entity.PersonByOption;

import java.util.*;

/**
 * @author 沈传尚
 * @date 2022/3/11
 */
public class MapDemo {
    public static void main(String[] args) {
        Map<Integer, List<PersonByOption>> map=new HashMap<>();

        PersonByOption p1 =new PersonByOption();
        p1.setApprovalPersonId(1);
        PersonByOption p2=new PersonByOption();
        p2.setApprovalPersonId(840);
        List<PersonByOption> list1 = Arrays.asList(p1, p2);

        PersonByOption p3=new PersonByOption();
        p3.setApprovalPersonId(1235);
        List<PersonByOption> list2 = Arrays.asList(p3);
        map.put(1,list1);
        map.put(2,list2);

        List<Set<Integer>> approvalList=new ArrayList<>();
        for (Map.Entry<Integer, List<PersonByOption>> entry : map.entrySet()) {
            List<PersonByOption> value = entry.getValue();
            Set<Integer> set = new HashSet<>();
            for (PersonByOption p : value) {
                set.add(p.getApprovalPersonId());
            }
            approvalList.add(set);
        }
        System.out.println(approvalList);

    }
}
