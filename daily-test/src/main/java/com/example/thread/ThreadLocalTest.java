package com.example.thread;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * @author 沈传尚
 * @date 2022/2/7
 */
public class ThreadLocalTest {
    public static void main(String[] args) {
        ThreadLocal<DateFormat> df = ThreadLocalTest.df;
        System.out.println(df);
    }

    public static final ThreadLocal<DateFormat> df=new ThreadLocal<DateFormat>(){
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };
}
