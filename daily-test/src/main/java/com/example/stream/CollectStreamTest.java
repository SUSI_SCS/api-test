package com.example.stream;


import com.example.entity.Area;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 沈传尚
 * @date 2022/2/18
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class CollectStreamTest {

    public List<Area> createObject(){
       return  Arrays.asList(Area.builder().city("滁州市").province("安徽省").build(),
               Area.builder().city("南京市").province("江苏省").build());
    }
    /**
     * 多级分组
     */
   @Test
    public void collectStreamTestOne(){
        List<Area> areas = createObject();
        Map<String, List<Area>> collect1 = areas.stream().collect(Collectors.groupingBy(Area::getProvince));
        System.out.println(collect1);
        Map<String, Map<String, List<Area>>> collect = areas.stream().collect(Collectors.groupingBy(Area::getCity, Collectors.groupingBy(Area::getProvince)));
        System.out.println(collect);
    }

    @Test
    public void test(){
        List<String> list = Arrays.asList("2021", "2022", "2023");
        StringBuilder builder=new StringBuilder();
        for (String s : list){
            builder.append(s).append("-");
        }
        String s = builder.deleteCharAt(builder.length()-1).toString();
        System.out.println(s);
    }
}
