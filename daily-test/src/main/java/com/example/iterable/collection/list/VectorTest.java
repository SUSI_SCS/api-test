package com.example.iterable.collection.list;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * @author 沈传尚
 * @date 2022/1/8
 */
public class VectorTest {
    public static void main(String[] args) throws Exception {
        List<Integer> list=new ArrayList<>();
        list.add(10);
        System.out.println(list.size());
        Field[] declaredFields = ArrayList.class.getDeclaredFields();

        for (Field f : declaredFields){
            String substring = f.toString().substring(f.toString().lastIndexOf(".")+1);
        }

        int capacity = getCapacity((ArrayList<?>) list);
        System.out.println(capacity);


    }

    // 获取一个集合的容量
    static int getCapacity(ArrayList<?> l) throws Exception {
        Field dataField = ArrayList.class.getDeclaredField("elementData");
        dataField.setAccessible(true);
        return ((Object[]) dataField.get(l)).length;
    }


}
