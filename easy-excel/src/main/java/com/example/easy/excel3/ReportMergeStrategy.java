package com.example.easy.excel3;

import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.write.merge.AbstractMergeStrategy;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 沈传尚
 * @date 2022/3/15
 */
public class ReportMergeStrategy extends AbstractMergeStrategy {
    //合并坐标集合
    private List<CellRangeAddress> cellRangeAddresss;

    //构造
    public ReportMergeStrategy(int num) {
        List<CellRangeAddress> list = new ArrayList<>();
        for (int k = 0; k < num; k++) {
            int i = k * 3 + 2;
            list.add(new CellRangeAddress(i, i + 2, 0, 0));
            list.add(new CellRangeAddress(i, i + 2, 5, 5));
            list.add(new CellRangeAddress(i, i + 2, 6, 6));
            list.add(new CellRangeAddress(i, i + 2, 7, 7));
            list.add(new CellRangeAddress(i, i + 2, 8, 8));
            list.add(new CellRangeAddress(i, i + 2, 9, 9));
            list.add(new CellRangeAddress(i, i + 2, 10, 10));
            list.add(new CellRangeAddress(i, i + 2, 11, 11));
            list.add(new CellRangeAddress(i, i + 2, 12, 12));
        }
        this.cellRangeAddresss = list;
    }

    /**
     * merge
     *
     * @param sheet
     * @param cell
     * @param head
     * @param relativeRowIndex
     */
    @Override
    protected void merge(Sheet sheet, Cell cell, Head head, Integer relativeRowIndex) {
        if (CollectionUtils.isNotEmpty(cellRangeAddresss)) {
            if (cell.getRowIndex() == 2 && cell.getColumnIndex() == 0) {
                for (CellRangeAddress item : cellRangeAddresss) {
                    sheet.addMergedRegionUnsafe(item);
                }
            }
        }
    }
}
