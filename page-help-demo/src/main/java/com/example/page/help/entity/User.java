package com.example.page.help.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 沈传尚
 * @since 2022-01-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_user")
public class User implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String username;

    /**
     * 首字母缩写
     */
    private String abbname;

    /**
     * 密码
     */
    private String password;

    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 工号
     */
    private String jobNumber;

    /**
     * 部门id
     */
    private Integer deptId;

    /**
     * 职位id
     */
    private Integer positionId;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 性别
     */
    private String sex;

    /**
     * 身份证号
     */
    private String personId;

    /**
     * 生日类型（阳历/阴历）
     */
    private String birthdayType;

    /**
     * 生日
     */
    private String birthday;

    /**
     * 民族
     */
    private String race;

    /**
     * 户口类型
     */
    private String householdType;

    /**
     * 婚育状况
     */
    private String maritalStatus;

    /**
     * 户籍
     */
    private String permanentAddress;

    /**
     * 家庭住址
     */
    private String homeAddress;

    /**
     * 政治面貌
     */
    private String politicsFace;

    /**
     * 职级ID
     */
    private Integer rankId;

    /**
     * 毕业时间
     */
    private String graduateDate;

    /**
     * 毕业学校
     */
    private String graduateSchool;

    /**
     * 学历ID
     */
    private Integer educateId;

    /**
     * 专业
     */
    private String major;

    /**
     * 职称
     */
    private String title;

    /**
     * 驾照领取时间
     */
    private String driveLicenseDate;

    /**
     * 外语水平
     */
    private String foreignLevel;

    /**
     * 兴趣及特长
     */
    private String hobby;

    /**
     * 计算机水平
     */
    private String computerLevel;

    /**
     * 俱乐部名
     */
    private String clubNames;

    /**
     * 工资卡号
     */
    private String cardNumber;

    /**
     * 用户状态0在职1离职2冻结
     */
    private Integer userStatus;

    /**
     * 入职时间
     */
    private String entryDate;

    /**
     * 离职时间
     */
    private String leaveDate;

    /**
     * 师傅ID
     */
    private Integer masterId;

    /**
     * 转正日期
     */
    private String conversionDate;

    /**
     * 合同起始时间
     */
    private String contractStartDate;

    /**
     * 合同到期时间
     */
    private String contractEndDate;

    /**
     * 社保起始时间
     */
    private String socialSecurityDate;

    /**
     * 未购买原因
     */
    private String notbuyReason;

    /**
     * 公积金开始时间
     */
    private String fundStartDate;

    /**
     * 删除状态位，默认是0，1代表删除
     */
    private Integer deleteStatus;

    /**
     * 身份证附件路径
     */
    private String personidUrl;

    /**
     * 证书附件路径
     */
    private String certificateUrl;

    /**
     * 其他图片路径
     */
    private String picUrl;

    /**
     * 其他文件路径
     */
    private String fileUrl;

    /**
     * 钉钉ID
     */
    @TableField("ddUserId")
    private String ddUserId;

    /**
     * 角色id
     */
    private Integer roleId;

    private LocalDate applyDate;

    private Boolean leaveType;

    private LocalDate salaryDate;

    private String leaveReason;

    private String note;

    private String ip;

    /**
     * 初始密码
     */
    private String initialPassword;

    /**
     * 合同照片路径
     */
    private String contractUrl;

    /**
     * 转岗日期
     */
    private LocalDate transferDate;

    /**
     * 是否转岗，1：是，0否
     */
    private Integer isTransfer;

    /**
     * 默认状态：-1全部、 0：实习、1：试用、2：转正、3：离职
     */
    private Integer positionStatus;

    /**
     * 职位当前状态（进度）：-1 正常，0：待离职（办理离职流程），1：待转正（办理转正流程中），2：待转岗（办理转岗中）
     */
    private Integer currentPositionStatus;

    /**
     * 是否在奖惩统计页展示,0:不展示
     */
    private Integer isShow;

    /**
     * 离职前的状态:  0：实习、1：试用、2：转正、3：离职
     */
    private Integer beforeLeaveStatus;


}
