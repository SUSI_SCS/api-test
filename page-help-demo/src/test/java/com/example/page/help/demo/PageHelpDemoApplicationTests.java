package com.example.page.help.demo;

import com.example.page.help.entity.User;
import com.example.page.help.mapper.UserMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Base64;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PageHelpDemoApplicationTests {

    @Autowired(required = false)
    private UserMapper userMapper;

    @Test
    public void findByIdTest(){
        User user = userMapper.selectById(1235);
        System.out.println(user);
    }


    @Test
    public void pageTest(){
        int pageNum=2;  //获取第二页的数据
        int pageSize=10; // 每页显示30条数据
        String orderBy="id";

        PageHelper.startPage(pageNum,pageSize,orderBy);

        // 执行分页查询
        PageInfo<User> userPageInfo=new PageInfo<>(userMapper.selectList(null));

        // 打印用户列表
        List<User> list = userPageInfo.getList();
        list.forEach(System.out::println);


    }

    @Test
    public void test(){
        double random = Math.random();
        double v = 0.3 - 0.1;
        String format = String.format("%.2f", v);
        System.out.println(format);
        System.out.println(v);


        System.out.println(v);
        double f=0.3f-0.1f;
        System.out.println(f);
        BigDecimal b1=new BigDecimal("0.3");
        BigDecimal b2=new BigDecimal("0.1");
        BigDecimal subtract = b1.subtract(b2);
        String s = subtract.toString();
        System.out.println(s);
        System.out.println(b1.subtract(b2));

        System.out.println(v);
        System.out.println(f);
    }


}




















