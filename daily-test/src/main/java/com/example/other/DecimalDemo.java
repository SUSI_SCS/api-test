package com.example.other;

import java.text.DecimalFormat;

/**
 * @author 沈传尚
 * @date 2022/4/24
 */
public class DecimalDemo {
    public static void main(String[] args) {
        float num= (float)1/2;
        DecimalFormat df = new DecimalFormat("0.00");//格式化小数
        String s = df.format(num);
        String substring = s.substring(s.indexOf(".")+1)+"%";
        System.out.println(substring);

    }
}
