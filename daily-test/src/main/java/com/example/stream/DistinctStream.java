package com.example.stream;

import cn.hutool.core.collection.CollUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author susi
 * @date 2022/5/17
 */
public class DistinctStream {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("1", "5", "10", "15", "5","3","15");
        List<String> s=new ArrayList<>(list);

        List<String> collect = s.stream().distinct().collect(Collectors.toList());
        System.out.println(collect);


    }
}
