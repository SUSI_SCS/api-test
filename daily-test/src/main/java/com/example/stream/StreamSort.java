package com.example.stream;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author 沈传尚
 * @date 2022/4/14
 */
public class StreamSort {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(50, 25, 16);
        Collections.sort(list);
        System.out.println(list);
    }
}
