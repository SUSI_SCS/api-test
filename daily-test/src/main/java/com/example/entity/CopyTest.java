package com.example.entity;

import org.springframework.beans.BeanUtils;

import java.util.Arrays;

/**
 * @author 沈传尚
 * @date 2022/4/18
 */
public class CopyTest {
    public static void main(String[] args) {
        Resource resource=new Resource();
        resource.setExaminerIds(Arrays.asList(1,2));
        Target target=new Target();

        BeanUtils.copyProperties(resource,target);

        System.out.println(target.getExaminerIds());
    }
}
