package com.bz.portal.controller;


import com.bz.portal.entity.SalaryModel;
import com.bz.portal.service.impl.SalaryModelServiceImpl;
import com.bz.portal.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 沈传尚
 * @since 2022-01-17
 */
@RestController
@RequestMapping("/portal/salary-model")
public class SalaryModelController extends BaseController{

    @Autowired
    private SalaryModelServiceImpl salaryModelService;


    @GetMapping(value = "/exception")
    public JsonResult exceptionTest(){
       return salaryModelService.exceptionTest();
    }

    @GetMapping("/page/{type}")
    public JsonResult list(@PathVariable(value = "type")Integer type){
        List<SalaryModel> salaryModels = salaryModelService.listAll(type);
        return new JsonResult(OK,"操作成功",salaryModels);
    }


}

