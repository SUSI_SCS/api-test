package com.example.reflect;

import com.example.entity.Person;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/*
(1)forName(String classname)
该方法返回给定串名相应的Class对象。

(2)getClassLoader() // 类装载器负责从Java字符文件将字符流读入内存，并构造Class类对象
获取该类的类装载器。

(3)getComponentType()
如果当前类表示一个数组，则返回表示该数组组件的Class对象，否则返回null。

(4)getConstructor(Class[])
返回当前Class对象表示的类的指定的公有构造子对象。

(5)getConstructors()
返回当前Class对象表示的类的所有公有构造子对象数组。

(6)getDeclaredConstructor(Class[])
返回当前Class对象表示的类的指定已说明的一个构造子对象。

(7)getDeclaredConstructors()
返回当前Class对象表示的类的所有已说明的构造子对象数组。

(8)getDeclaredField(String)
返回当前Class对象表示的类或接口的指定已说明的一个域对象。

(9)getDeclaredFields()
返回当前Class对象表示的类或接口的所有已说明的域对象数组。

(10)getDeclaredMethod(String,Class[])
返回当前Class对象表示的类或接口的指定已说明的一个方法对象。
*/
public class ApiDemo {
    public static void main(String[] args) throws ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<?> clazz = Class.forName("com.example.entity.Person");

        ClassLoader classLoader = clazz.getClassLoader();

        Person p1=null;
        Constructor<?>[] constructors = clazz.getConstructors();
        p1 = (Person) constructors[0].newInstance(5,5);
        System.out.println(p1);

    }
}
