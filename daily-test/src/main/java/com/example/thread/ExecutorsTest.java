package com.example.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author 沈传尚
 * @date 2022/1/25
 */
public class ExecutorsTest {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        System.out.println(Thread.currentThread().getName());

        for (int i = 0; i < 6; i++) {
            final int j=i;
            executorService.execute(()->{
                System.out.println(Thread.currentThread().getName()+" "+j);
            });
        }


        executorService.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        });

        executorService.shutdown();
    }
}
