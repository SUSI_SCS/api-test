package com.example.dailytest;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 沈传尚
 * @date 2022/4/22
 */
public class RanDemo {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("10", null);

        List<String> collect = list.stream().filter(Objects::nonNull).collect(Collectors.toList());

        System.out.println(collect);
    }



}
