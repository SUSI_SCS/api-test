package com.example.iterable.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 数组转换为List集合
 * 数组的工具类Arrays提供了一个静态方法:asList,可以将一个数组转换为一个List集合
 *
 * @author 沈传尚
 * @date 2021/12/23
 */
public class ArrayToList {
    public static void main(String[] args) {
        // 1.0数组装换为list集合
        String[] array={"a","b","f","g"};
        List<String> list = Arrays.asList(array);
        // 注意: 将数组转为集合后 不能对该集合做增删操作 因为数组是定长的 否则会出现"UnsupportedOperationException"的异常
        // list.add("h");
        // list.remove("a");
        // 若想增删元素,只能自行创建一个集合并包含数组转换的集合中 所有元素后再进行相关操作
        List<String> list1=new ArrayList<>();
        list1.addAll(list);
        System.out.println(list);

        // 2.0 将数组变为字符串
        String s = Arrays.toString(array);
        System.out.println(s);

        // 3.0 替换集合指定位置的元素
        list.set(0,"c");
        System.out.println(list);

        // 4.0 所有的集合都支持一个参数为Collection的构造方法,作用是在创建当前集合的同时包含给定集合中的所有元素
        List<String> list2=new ArrayList<>(list);
        System.out.println(list2);

    }
}
