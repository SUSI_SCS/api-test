package com.example.stream;

/**
 * @author 沈传尚
 * @date 2022/2/18
 */



import com.example.entity.Point;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 采用stream流获取集合对象的某一属性
 */
public class GetObjectAttribute {
    public static void main(String[] args) {
        List<Point> list = Arrays.asList(Point.builder().x(5).y(10).build(),
                Point.builder().x(2).y(14).build());

        List<Integer> XList = list.stream().map(Point::getX).collect(Collectors.toList());
        System.out.println(list);
        System.out.println(XList);
    }
}
