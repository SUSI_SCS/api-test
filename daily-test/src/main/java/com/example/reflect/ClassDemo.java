package com.example.reflect;
/**
 * 获取Class对象的三种方法
 * 1.使用Class类的静态方法
 * eg：Class.forName(“java.lang.String”);
 *
 * 2.使用类的.class语法
 * eg：Class c = Employee.class;
 *
 * 3.使用对象的getClass()方法
 * eg：Employee e = new Employee();  Class c3 = e.getClass();
 * */
public class ClassDemo {
    public static void main(String[] args) throws ClassNotFoundException {
        Class<?> clazz1 = Class.forName("java.lang.String");

        Class<String> clazz2 = String.class;

        String s="abc";

        Class<? extends String> clazz3 = s.getClass();

        System.out.println(clazz1 == clazz2);

        System.out.println(clazz1 == clazz3);

    }

}
