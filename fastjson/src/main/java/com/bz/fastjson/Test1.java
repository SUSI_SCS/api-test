package com.bz.fastjson;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 沈传尚
 * @date 2022/1/19
 */
public class Test1 {
    public static void main(String[] args) {
        Map<String,Student> map=new HashMap<>();
        map.put("student",Student.builder().name("张三").age(18).id(1).build());

        // JSON.toJSONString 序列化 (对象->json); JSON.parseObject 反序列化(json -> 对象) 需要提供无参/全参 构造方法
        Student student = JSON.parseObject(JSON.toJSONString(map.get("student")), Student.class);

        System.out.println(student);


    }
}
