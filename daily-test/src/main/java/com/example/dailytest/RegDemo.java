package com.example.dailytest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 沈传尚
 * @date 2022/4/20
 */
public class RegDemo {
    static String reg="^[1-9][0-9]/d*/s[1-9]/d*$";

    String s="100 100";

    public static void main(String[] args) {
        boolean reg = reg("10 1000");
        System.out.println(reg);
    }


    public static boolean reg(String s){
        Pattern pattern = Pattern.compile(reg);
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }

}
