package com.example.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 沈传尚
 * @date 2022/3/11
 */
@Data
public class PersonByOption {

    /**
     * 责任人
     */
    @NotNull(message = "责任人不能为空")
    private Integer chargePersonId;

    /**
     * 审批人
     */
    public Integer approvalPersonId;

    /**
     * 审批级别
     */
    protected Integer level;


    private Integer age;


}
