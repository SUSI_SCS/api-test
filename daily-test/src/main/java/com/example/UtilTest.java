package com.example;

import com.example.util.CalendarUtils;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author 沈传尚
 * @date 2022/3/26
 */
public class UtilTest {
    public static void main(String[] args) {
        // 判断该年是否有闰月
        int curYear = LocalDate.now().getYear();
        boolean isLeapMonth = curYear % 4 == 0 && !(curYear % 100 == 0) || curYear % 400 == 0;

        int[] ints = CalendarUtils.lunarToSolar(1999, 10, 4, isLeapMonth);
        for (int i : ints){
            System.out.println(i);
        }

    }
}
